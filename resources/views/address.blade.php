<!DOCTYPE html>
<head>
    <title>Address Verification</title>
    <meta charset="utf-8"> 
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script><!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.12/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.12/js/jquery.dataTables.js"></script>

    <style type="text/css">
        #addressForm input{
            margin: 5px;
        }
        .error {
            color: red;
            display: none;
        }
    </style>

    <script type="text/javascript">

    $(document).ready( function () {

        var data = [    
            @foreach ($data as $key => $value)
            [
                '{{ $value['street'] }}',
                '{{ $value['city'] }}',
                '{{ $value['state'] }}',
                '{{ $value['zip'] }}'
            ],
            @endforeach
        ];

        var table = $('#table_id').DataTable({
            data : data
        });

        $('#verify').on('click', function() {
            $('.error').css('display', 'none');
            $.ajax({
                method: "POST",
                url: "/validate",
                dataType: "json",
                data: $('#addressForm').serialize()
            })
            .done(function(response) {
                if ( response.street.length > 0 ) {
                    table.row.add(
                        [
                            response.street,
                            response.city,
                            response.state,
                            response.zip
                        ]
                    ).draw();
                } else {
                    $('.error').show();
                    $('input[type="text"]').val('');
                    $('input[name="street"]').focus();
                }
            })
            .fail(function() {
                $('.error').show();
                $('input[type="text"]').val('');
                $('input[name="street"]').focus();
            });
        });
    });


    </script>

</head>
<body>

    <div class="container">
      <div class="row">
        <div class="col-md-8 col-sm-12">
          <h2>Verified Addresses</h2>
        </div>
      </div>

      <div class="row">
        <div class="col-md-8 col-sm-12">
            <table id="table_id" class="display">
                <thead>
                    <tr>
                        <th>Street</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Zip</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
      </div>

    {!! Form::open(['url' => '', 'method' => '', 'id' => 'addressForm', 'onsubmit' => 'return false;']) !!}
      <div class="row">
        <div class="col-md-8 col-sm-12">
          <h2>Verification Form</h2>
          <div class="row">
            <div class="col-md-8 col-sm-12">
              <p class="error">Invalid Address Entered. Please try again.</p>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
                <label>Street</label>
            </div>
            <div class="col-md-6">
                <input type="text" name="street" value="">
            </div>

            <div class="col-md-6">
                <label>City</label>
            </div>
            <div class="col-md-6">
                <input type="text" name="city" value="">
            </div>

            <div class="col-md-6">
                <label>State</label>
            </div>
            <div class="col-md-6">
                <input type="text" name="state" value="">
            </div>

            <div class="col-md-6">
                <button id="verify">Verify Address</button>
            </div>
            <div class="col-md-6">
                    
            </div>
          </div>
        </div>
      </div>
    {!! Form::close() !!}
    </div>

</body>
</html>