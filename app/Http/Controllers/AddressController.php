<?php

namespace App\Http\Controllers;

use App\Models\Addresses;
use App\Factories\Address;
use Illuminate\Http\Request;
use Exception;

class AddressController extends Controller
{

    protected $addresses;
    protected $ss;
    protected $google;


    public function __construct(Addresses $addresses, Address $address)
    {

        $this->addresses = $addresses;
        $this->address = $address;
    }


    public function index()
    {
        $addresses = $this->addresses->get();
        $addr = [];
        $i = 0;
        foreach ($addresses as $key => $value) {
            $data = json_decode($value->user_input, 1);
            $addr[$i]['street'] = $data['street'];
            $addr[$i]['city'] = $data['city'];
            $addr[$i]['state'] = $data['state'];
            $addr[$i]['zip'] = $value->zip;
            $i++;
        }

        return view('address', ['data' => $addr]);
    }

    public function addressValidate(Request $request)
    {
        $params = $request->all();

        try {
            $response = $this->address->create('Google')->validateAddress($params);

            if (!empty($response) && $response['success']) {
                $response = $this->responseProcess($response, $params);
                echo json_encode($response);
                exit();
            }

            $response = $this->address->create('SS')->validateAddress($params);

            if (!empty($response) && $response['success']) {
                $response = $this->responseProcess($response, $params);
                echo json_encode($response);
                exit();
            }

            echo '';
            exit();
        } catch (Exception $err) {
            echo '';
            exit();
        }
    }

    public function responseProcess($response, $params)
    {
        $payload = $response['payload'];

        $this->addresses->user_input = json_encode($params);
        $this->addresses->zip = $response['zip'];
        $this->addresses->api_response = $payload;
        $this->addresses->save();

        unset($response['payload']);

        return $response;
    }
}