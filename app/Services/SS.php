<?php

namespace App\Services;

use App\Services\CurlService;

class SS extends CurlService
{
    function validateAddress($params)
    {
        $url = 'https://api.smartystreets.com/street-address';
        $params['auth-id'] = env('SMART_ID');
        $params['auth-token'] = env('SMART_TOKEN');
        $responseArray = $this->curl($params, $url);

        if (!empty($responseArray[0]['components'])) {
            $responseComponents = $responseArray[0]['components'];
            $response['success'] =  true;
            $response['street'] = $responseComponents['primary_number'] . ' ' 
                . (!empty($responseComponents['street_predirection'])? $responseComponents['street_predirection'] . ' ' : '') 
                . $responseComponents['street_name'] . ' ' . $responseComponents['street_suffix'];
            $response['city'] = $responseComponents['city_name'];
            $response['state'] = $responseComponents['state_abbreviation'];
            $response['zip'] = $responseComponents['zipcode'];
            $response['payload'] = $responseArray['payload'];
        } else {
            $response['success'] = false;
        }
        return $response;
    }

}
