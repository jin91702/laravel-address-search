<?php

namespace App\Services;

use \Exception;

class CurlService 
{

    public function curl($params, $url)
    {
        // Curl call to api
        $ch = curl_init();

        if (!empty($params) && count($params) > 0) {
            $url .= '?' . http_build_query($params);

            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $responseRaw = curl_exec($ch);
            curl_close($ch);
        }
        $responseArray = json_decode($responseRaw, true);
        $responseArray['payload'] = $responseRaw;
        return $responseArray;
    }
}
