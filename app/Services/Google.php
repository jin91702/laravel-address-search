<?php

namespace App\Services;

use App\Services\CurlService;

class Google extends CurlService
{
    function validateAddress($params)
    {
        $url = 'https://maps.googleapis.com/maps/api/geocode/json';
        $params['key'] = env('GOOGLE_KEY');
        $params['address'] = $params['street'] . ', ' . $params['city'] . ', ' . $params['state'];
        $responseArray = $this->curl($params, $url);
    
        $components = [];
        if (!empty($responseArray['results'][0]['address_components'])) {
            $addressComponents = $responseArray['results'][0]['address_components'];
            foreach ($addressComponents as $key => $value) {
                if (!empty($value['types'][0])) {
                    $components[$value['types'][0]] = $value['short_name'];
                }
            }
        }

        if (count($components) == 8) {
            $response['success'] =  true;
            $response['street'] = $components['street_number'] . ' ' .$components['route'];
            $response['city'] = $components['locality'];
            $response['state'] = $components['administrative_area_level_1'];
            $response['zip'] = $components['postal_code'];
            $response['payload'] = $responseArray['payload'];
        } else {
            $response['success'] = false;
        }
        return $response;
    }

}