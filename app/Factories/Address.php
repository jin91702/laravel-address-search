<?php

namespace App\Factories;

use \Exception;

class Address 
{
    const CLASS_NAMESPACE = 'App\Services';

    public static function create($provider)
    {
        try {
            $className = implode('\\', [self::CLASS_NAMESPACE, $provider]);

            return new $className();
        } catch (Exception $e) {

        }
    }
}
